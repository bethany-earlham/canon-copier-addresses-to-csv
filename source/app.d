import std.stdio;

void main()
{
	import std.algorithm.iteration : filter;
	import std.stdio : File, toFile;
	import std.string : chompPrefix, startsWith, stripRight;

	// TODO: Take these on the command-line.
	string path = "abook.abk";
	string output_path = "addresses.csv";

	auto text = File(path).byLine
		.filter!(a => a.startsWith("cn: ") || a.startsWith("mailaddress: "));
	string[] csv;
	string current_line;
	foreach (line; text) {
		if (line.startsWith("cn: ")) {
			current_line = "\"" ~
				cast(string)(line).chompPrefix("cn: ").dup.stripRight ~ "\"";
		} else if (line.startsWith("mailaddress: ")) {
			current_line ~= "," ~ cast(string)(line).chompPrefix("mailaddress: ") ~ "\n";
			csv ~= current_line;
		} else throw new Exception("Unexpected line: " ~ cast(string)line);
	}

	csv.toFile(output_path);
}
